package serie06;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import serie06.event.SentenceEvent;
import serie06.event.SentenceListener;
import serie06.model.AbstractActor;
import serie06.model.Box;
import serie06.model.StdConsumer;
import serie06.model.StdProducer;
import serie06.model.UnsafeBox;
import serie06.util.Formatter;

public class ProdCons {
	
	// CONSTANTES
	
	private static final int MAX_ITER = 10;
	private static final int FRAME_WIDTH = 800;
	private static final int FRAME_HEIGHT = 600;
	private static final int FONT_SIZE = 12;
	private static final int THREAD_SLEEP_TIME = 1000;
	
	// ATTRIBUTS
	
	private JFrame frame;
	private JTextArea textArea;
	private JButton startButton;
	private AbstractActor[] actors;
	private Box box;
	private Map<AbstractActor, Formatter> formatters;
	
	// CONSTRUCTEUR
	
	public ProdCons() {
		createModel();
		createView();
		placeComponents();
		createController();
	}
	
	// COMMANDES
	
	public void display() {
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	// OUTILS
	
	private void createModel() {
		box = new UnsafeBox();
		
		actors = new AbstractActor[] {
			new StdProducer(box, MAX_ITER), 
			new StdProducer(box, MAX_ITER),
			new StdProducer(box, MAX_ITER), 
			new StdConsumer(box, MAX_ITER), 
			new StdConsumer(box, MAX_ITER)
		};
		
		formatters = new HashMap<AbstractActor, Formatter>();
		
		int consumers = 1;
		int producers = 1;
		
		for (AbstractActor actor : actors) {
			if (actor instanceof StdConsumer) {
				formatters.put(actor, 
						new Formatter("C" + Integer.toString(consumers)));
				consumers++;
			} else {
				formatters.put(actor, 
						new Formatter("P" + Integer.toString(producers)));
				producers++;
			}
		}
	}
	
	private void createView() {
		frame = new JFrame("Producteurs & Consommateurs");
		frame.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
		
		startButton = new JButton("Démarrer");
		
		textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, FONT_SIZE));
		textArea.setEditable(false);
	}
	
	private void placeComponents() {
		
		JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER)); {
			p.add(startButton);
		}
		
		frame.add(p, BorderLayout.NORTH);
		
		frame.add(new JScrollPane(textArea), BorderLayout.CENTER);
	}
	
	private void createController() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		for (AbstractActor a : actors) {
			a.addSentenceListener(new SentenceListener() {

				public void sentenceSaid(SentenceEvent e) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							textArea.append(
									formatters.get(a).format(e.getSentence()));
						}
					});
				}
			});
		}
		
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				startButton.setEnabled(false);
				textArea.append("--------------------------\n");
				
				Formatter.resetTime();
				for (AbstractActor a : actors) {
					a.start();
				}
				
				
				Thread checkThreads = new Thread(new ThreadsBlocked());
				checkThreads.start();
			}
		});
	}
	
	// POINT D'ENTREE
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ProdCons().display();
			}
		});
	}
	
	// CLASSE INTERNE
	
	private class ThreadsBlocked implements Runnable {

		// ATTRIBUTS
		
		private boolean actorsAlive;
		private boolean quit;
		
		// COMMANDES

		public void run() {
			while (!quit) {
				
				actorsAlive = false;
				
				for (AbstractActor a : actors) {
					if (a.isActive() && !a.isWaitingOnBox()) {
							actorsAlive = true;
					}
				}
				
				if (!actorsAlive) {
					
					for (AbstractActor a : actors) {
						if (a.isActive() && a.isWaitingOnBox()) {
							a.interruptAndWaitForInactivation();
						}
					}
					
					startButton.setEnabled(true);
					
					quit = true;
				}
				
				try {
					Thread.sleep(THREAD_SLEEP_TIME);
				} catch (InterruptedException e) {
				}
			}
		}
	}
}

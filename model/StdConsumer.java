package serie06.model;

public class StdConsumer extends AbstractActor {
	
	// ATTRIBUTS
	
	private Box box;

	public StdConsumer(Box box, int maxIterNb) {
		super(box, maxIterNb);
		
		this.box = box;
	}
	
	protected boolean canUseBox() {
		return !box.isEmpty();
	}

	@Override
	protected void useBox() {
		synchronized (box) {
			fireSentenceSaid("box --> " + box.getValue());
			box.dump();
		}
	}
}

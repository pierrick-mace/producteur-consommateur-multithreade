package serie06.model;

import serie06.event.SentenceListener;
import serie06.event.SentenceSupport;
import util.Contract;

public abstract class AbstractActor {
	
	private static Object lock;
	
	static {
		lock = new Object();
	}
	
	// ATTRIBUTS
	
	private Box box;
	private int maxIterNb;
	private SentenceSupport listeners;
	private TaskCode task;
	private boolean running;
	
	
	// CONSTRUCTEUR
	public AbstractActor(Box box, int maxIterNb) {
		this.box = box;
		this.maxIterNb = maxIterNb;
		listeners = new SentenceSupport(this);
		task = new TaskCode(box, maxIterNb);
	}
	
	
	// REQUETES
	
	public Box getBox() {
		return box;
	}
	
	public int getMaxIterNb() {
		return maxIterNb;
	}
	
	public SentenceListener[] getSentenceListeners() {
		return listeners.getSentenceListeners();
	}
	
	public boolean isStarted() {
		return running;
	}
	
	public boolean isStopped() {
		return !running;
	}
	
	public boolean isActive() {
		return task.isAlive();
	}
	
	public boolean isWaitingOnBox() {
		return task.isWaitingOnBox();
	}
	
	protected abstract boolean canUseBox();
	protected abstract void useBox();
	
	// COMMANDES
	
	public void addSentenceListener(SentenceListener listener) {
		Contract.checkCondition(listener != null);
		
		listeners.addSentenceListener(listener);
	}
	
	public void removeSentenceListener(SentenceListener listener) {
		Contract.checkCondition(listener !=  null);
		
		listeners.removeSentenceListener(listener);
	}
	
	public void start() {
		Contract.checkCondition(!isActive());
		
		task = new TaskCode(box, maxIterNb);
		running = true;
		task.start();
	}
	
	public void interruptAndWaitForInactivation() {
		Contract.checkCondition(isActive());
		
		running = false;
		task.stopped = true;
		
		task.interrupt();
		
		while (isActive()) {
			try {
				task.join();
			} catch (InterruptedException e) {
				task.interrupt();
			}
		}
	}
	
	// OUTILS
	
	protected void fireSentenceSaid(String sentence) {
		synchronized (lock) {
			listeners.fireSentenceSaid(sentence);
		}
	}
	
	
	// CLASSE INTERNE
	
	private final class TaskCode extends Thread {
		
		// ATTRIBUTS
		
		private boolean isWaitingOnBox;
		private Box box;
		private int maxIterNb;
		private boolean stopped;
			
		// CONSTRUCTEUR
		
		private TaskCode(Box box, int maxIterNb) {
			this.box = box;
			this.maxIterNb = maxIterNb;
		}
		
		// REQUETES
		
		private boolean isWaitingOnBox() {
			return isWaitingOnBox;
		}
		
		// COMMANDES
				
		public void run() {
			stopped = false;
			
			int i = 0;
			fireSentenceSaid("Naissance");
			
			while (i < maxIterNb && !stopped) {
				i++;
				fireSentenceSaid("Début de l'étape " + i);
				
				synchronized (box) {
					while (!canUseBox() && !stopped) {
						
						fireSentenceSaid("Suspendu");
						
						try {
							isWaitingOnBox = true;
							box.wait();
						} catch (InterruptedException e) {
							
						}
					
						isWaitingOnBox = false;
						fireSentenceSaid("Réactivé");
					}
				}
				
				synchronized (box) {
					if (canUseBox()) {			
						useBox();
						box.notifyAll();
					}
				}
				
				fireSentenceSaid("Fin de l'étape " + i);
			}
			
			if (stopped) {
				fireSentenceSaid("Mort subite");
			} else {
				fireSentenceSaid("Mort naturelle");
			}
		}
	}
}

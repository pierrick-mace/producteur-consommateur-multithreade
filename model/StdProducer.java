package serie06.model;

public class StdProducer extends AbstractActor {
	
	// CONSTANTES
	
	private static final int MIN = 1;
	private static final int MAX = 100;

	// ATTRIBUTS
	
	private Box box;
	
	// CONSTRUCTEUR
	
	public StdProducer(Box box, int maxIterNb) {
		super(box, maxIterNb);
		
		this.box = box;
	}
	
	
	@Override
	protected boolean canUseBox() {
		return box.isEmpty();
	}

	@Override
	protected void useBox() {
		synchronized (box) {
			int fillValue = MIN + (int) (Math.random() * (MAX - MIN + 1));
			box.fill(fillValue);
			fireSentenceSaid("box <-- " + fillValue);
		}
	}
}

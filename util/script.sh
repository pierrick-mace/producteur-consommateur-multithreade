FILES=*.java
for f in $FILES
do
	filename="${f%.*}"
	echo -n "$f"

	if file -i $f | grep -wq "iso-8859-1"
	then
		mkdir -p converted
		cp $f ./converted
		iconv -f ISO-8859-1 -t UTF-8 $f > "${filename}_utf8.java"
		mv "${filename}_utf8.java" $f
		echo "Converted to UTF-8"
	else
		echo "Not converted (Already UTF-8)"
	fi
done
